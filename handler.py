import logging
import os
from scroll_wow import get_app
from scroll_wow import lambda_handler as handler

_SAVE_LOAD_TIME = get_app()  # should help on warm lambda


if os.environ.get("LAMBDA_TASK_ROOT"):
    new_path = f"{os.environ['PATH']}:{os.environ['LAMBDA_TASK_ROOT']}/opt"
    logging.warning(f"PATH var set to: {new_path}")
    os.environ["PATH"] = new_path


def lambda_handler(event, context):
    logging.info({"event": event, "context": context})
    return handler(event, context)
