import os
import time
# import shutil
import subprocess
# import logging
from pathlib import Path
from typing import List, Optional, Union

__all__ = ["gifsicle", "optimize"]


# IS_LAMBDA = True if os.environ.get('LAMBDA_TASK_ROOT') else False
# LAMBDA_TASK_ROOT = os.environ.get('LAMBDA_TASK_ROOT', os.path.dirname(os.path.abspath(__file__)))
# CURR_BIN_DIR = os.path.join(LAMBDA_TASK_ROOT, 'bin')
# LIB_DIR = os.path.join(LAMBDA_TASK_ROOT, 'lib')
# # In order to get permissions right, we have to copy them to /tmp
# BIN_DIR = '/tmp/bin'


# # This is necessary as we don't have permissions in /var/tasks/bin
# # where the lambda function is running
# def _init_bin(executable_name):
#     start = time.perf_counter()
#     if not os.path.exists(BIN_DIR):
#         logging.info("Creating bin folder")
#         os.makedirs(BIN_DIR)
#     logging.info(f"Copying binaries for {executable_name} in /tmp/bin")
#     currfile = os.path.join(CURR_BIN_DIR, executable_name)
#     newfile = os.path.join(BIN_DIR, executable_name)
#     shutil.copy2(currfile, newfile)
#     logging.info("Giving new binaries permissions for lambda")
#     os.chmod(newfile, 0o775)
#     end = time.perf_counter()
#     logging.info(f"{executable_name} ready in {end-start:0.4f}s.")


def gifsicle(
    sources: Union[List[str], str, List[Path], Path],
    destination: Optional[str] = None,
    optimize: bool = False,
    colors: int = 256,
    options: Optional[List[str]] = None,
) -> None:
    """Apply gifsickle with given options to image at given paths.
    Parameters
    -----------------
    sources:Union[List[str], str, List[Path], Path],
        Path or paths to gif(s) image(s) to optimize.
    destination:Optional[str] = None
        Path where to save updated gif(s).
        By default the old image is overwrited.
        If multiple sources are specified, they will be merged.
    optimize: bool = False,
        Boolean flag to add the option to optimize image.
    colors:int = 256,
        Integer value representing the number of colors to use. Must be a power of 2.
    options:Optional[List[str]] = None
        List of options.
    Raises
    ------------------
    ValueError:
        If gifsicle is not installed.
    ValueError:
        If given source path does not exist.
    ValueError:
        If given source path is not a gif image.
    ValueError:
        If given destination path is not a gif image.
    References
    ------------------
    You can learn more about gifsicle at the project home page:
    https://www.lcdf.org/gifsicle/
    """
    if isinstance(sources, (str, Path)):
        sources = [sources]
    for source in sources:
        if isinstance(source, Path):
            source = str(source)  # should work on all windows, mac, and linux
        if not os.path.exists(source):
            raise ValueError("Given source path `{}` does not exist.".format(source))
        if not source.endswith(".gif"):
            raise ValueError("Given source path `{}` is not a gif image.".format(source))
    if destination is None:
        destination = sources[0]
    if not str(destination).endswith(".gif"):
        raise ValueError("Given destination path is not a gif image.")
    if options is None:
        options = []
    if optimize and "--optimize" not in options:
        options.append("--optimize")
    try:
        # if IS_LAMBDA:
        #     _init_bin("gifsicle")
        #     cmd = [
        #         os.path.join(BIN_DIR, "gifsicle"),
        #         *options,
        #         *sources,
        #         "--colors",
        #         str(colors),
        #         "--output",
        #         destination
        #     ]
        # else:
        #     cmd = ["gifsicle", *options, *sources, "--colors", str(colors), "--output", destination]
        cmd = ["gifsicle", *options, *sources, "--colors", str(colors), "--output", destination]
        subprocess.call(cmd, shell=False, stderr=subprocess.STDOUT)
    except FileNotFoundError:
        raise FileNotFoundError(
            (
                "The gifsicle library was not found on your system.\n"
                "On MacOS it is automatically installed using brew when you "
                "use the pip install command.\n"
                "On other systems, like Linux systems and Windows, it prompts the "
                "instructions to be followed for completing the installation.\n"
                "You can learn more on how to install gifsicle on "
                "the gifsicle and pygifsicle documentation."
            )
        )


def optimize(source: Union[str, Path], *args, **kwargs) -> None:
    """Optimize given gif.
    Parameters
    -----------------
    source:Union[str, Path],
        Path to gif image to optimize.
    """
    gifsicle(source, *args, **kwargs, optimize=True)
