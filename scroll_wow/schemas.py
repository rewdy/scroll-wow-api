from marshmallow import Schema, fields, validate
from marshmallow.validate import Range

DEFAULT_TEXT = "#ed40c5"
DEFAULT_BG = "#ffffff"


valid_hex_error = "Please enter a color as a hex value in the form of #ffffff."
valid_hex = validate.Regexp(r"^#[a-zA-Z0-9]{6}$", error=valid_hex_error)


class TextGifRequest(Schema):
    text = fields.Str(required=True)
    color = fields.Str(
        default=DEFAULT_TEXT,
        missing=DEFAULT_TEXT,
        validate=valid_hex,
    )
    bgcolor = fields.Str(
        default=DEFAULT_BG,
        missing=DEFAULT_BG,
        validate=valid_hex,
    )
    size = fields.Integer(
        validate=Range(min=10, min_inclusive=True, max=200, max_inclusive=True)
    )
