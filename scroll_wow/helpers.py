import os
import bottle
import logging
import json
import functools


# enable cors decorator
def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        bottle.response.headers["Access-Control-Allow-Origin"] = "*"
        bottle.response.headers["Access-Control-Allow-Methods"] = "OPTIONS, GET, POST"
        bottle.response.headers[
            "Access-Control-Allow-Headers"
        ] = "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token"

        if bottle.request.method != "OPTIONS":
            return fn(*args, **kwargs)

    return _enable_cors


# decorator to return response as json
def json_response(status_code=200):
    def _inner_func(func):
        @functools.wraps(func)
        def _json_response(*args, **kwargs):
            resp = func(*args, **kwargs)
            bottle.response.content_type = "application/json"
            bottle.response.status = status_code
            return json.dumps(resp)

        logging.info(_json_response)
        return _json_response

    return _inner_func


def get_font_path(project_path):
    """
    If we're deployed, then make an absolute path. Otherwise, return the
    relative path. Relative path must include project directory, i.e.
    "scroll_wow"
    """
    use_abs_path = os.environ.get("SLS_STAGE", "local") in ["dev", "prod"]
    return f"/var/task{project_path}" if use_abs_path else project_path
