from apig_wsgi import make_lambda_handler
from scroll_wow.app import make_app

_APP = None


def get_app():
    global _APP
    if _APP is None:
        _APP = make_app()
    return _APP


lambda_handler = make_lambda_handler(get_app(), binary_support=True)
