import json

import bottle
from apispec import APISpec
from apispec_webframeworks.bottle import BottlePlugin


def make_app():
    app = bottle.default_app()

    spec = APISpec(
        title="scroll-wow-v1",
        version="1.0.0",
        openapi_version="3.0.2",
        plugins=[BottlePlugin()],
        servers=[dict(url="/api/v1")],
    )

    _register_routes(app)
    _document_endpoints(app, spec)

    return app


def _register_routes(app):
    from scroll_wow import routes

    return routes


def _document_endpoints(app, spec, exclude=()):
    exclusion_set = set(exclude)
    for route in app.routes:
        matched = any(word in route.rule for word in exclusion_set)
        if not matched:
            spec.path(path=route.rule, view=route.callback)

    def dump_spec():
        return json.dumps(spec.to_dict())

    app.route("/spec")(dump_spec)
    app.route("/v1/spec")(dump_spec)


if __name__ == "__main__":
    bottle.run(make_app(), host="localhost", port=8080, debug=True, reloader=True)
