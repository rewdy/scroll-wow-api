import io
import logging
import tempfile
from base64 import b64encode
from random import randrange

from PIL import Image, ImageDraw, ImageFont

from scroll_wow.helpers import get_font_path
from scroll_wow.gifsicle import optimize

FONT_PATH = get_font_path("/scroll_wow/fonts/open-sans/OpenSans-BoldItalic.ttf")


def _get_text_width(text_string, font):
    return font.getmask(text_string).getbbox()[2]


def _hex_to_rgb(hex):
    if "#" in hex:
        h = hex.lstrip("#")
        return tuple(int(h[i : i + 2], 16) for i in (0, 2, 4))
    else:
        logging.warn("Can't convert; not a hex color.")
        return hex


def make_test_gif():
    images = []

    width = 200
    center = width // 2
    color_1 = (randrange(255), randrange(255), randrange(255))
    color_2 = (randrange(255), randrange(255), randrange(255))
    max_radius = int(center * 1.5)
    step = 16
    duration = 90

    for i in range(0, max_radius, step):
        im = Image.new("RGB", (width, width), color_1)
        draw = ImageDraw.Draw(im)
        draw.ellipse((center - i, center - i, center + i, center + i), fill=color_2)
        images.append(im)

    for i in range(0, max_radius, step):
        im = Image.new("RGB", (width, width), color_2)
        draw = ImageDraw.Draw(im)
        draw.ellipse((center - i, center - i, center + i, center + i), fill=color_1)
        images.append(im)

    img_io = io.BytesIO()

    images[0].save(
        img_io,
        format="GIF",
        save_all=True,
        append_images=images[1:],
        optimize=False,
        duration=duration,
        loop=0,
    )

    img_io.seek(0)

    return img_io


def make_text_gif(
    returns="json",
    text=None,
    color=None,
    bgcolor=None,
    duration=70,
    transparency=None,
    size=200,
):
    # Config
    font_size_in_points = round(size * 0.75)
    color = _hex_to_rgb(color)
    bgcolor = _hex_to_rgb(bgcolor)
    font = ImageFont.truetype(FONT_PATH, size=font_size_in_points)
    width = (
        size + _get_text_width(text, font) + size + size
    )  # for the space before and after
    duration = round((len(text) / 3) * 10) if not duration else duration

    # create image
    full_img = Image.new("RGB", (width, size), color=bgcolor)
    # draw text
    d = ImageDraw.Draw(full_img)
    d.text((size, 0), text, font=font, fill=color)

    # now, we create gif from crops
    images = []
    adjusted_width = width - size
    offset_per_step = round(0.125 * size)
    steps = round(adjusted_width / offset_per_step)

    for i in range(0, steps):
        left = round(offset_per_step * i)
        top = 0
        right = left + size
        bottom = size
        im = full_img.crop((left, top, right, bottom))
        bg_im = Image.new("RGB", (size, size), color=bgcolor)
        bg_im.paste(im, (0, 0))
        images.append(bg_im)

    # Create a tempfile to save our gif
    img_temp = tempfile.NamedTemporaryFile(suffix=".gif")
    # Save gif
    images[0].save(
        img_temp,
        format="GIF",
        save_all=True,
        append_images=images[1:],
        optimize=False,
        duration=duration,
        loop=0,
        disposal=2,
    )

    # Reset marker to the beginning
    img_temp.seek(0)

    # Optimize with gifsicle
    optimize(img_temp.name)

    # Get contents and close the tempfile
    img_data = img_temp.read()
    img_temp.close()

    # Return it
    if returns == "gif":
        return img_data
    else:
        return {"image": b64encode(img_data).decode()}
