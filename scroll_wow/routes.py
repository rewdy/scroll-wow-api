import logging
import json

import bottle
from webargs.bottleparser import use_kwargs
from scroll_wow.helpers import enable_cors
from scroll_wow.images import make_test_gif, make_text_gif
from scroll_wow.schemas import TextGifRequest


@bottle.route("/api/v1/gif", method="GET")
@enable_cors
def get_gif():
    """
    Returns a gif. This is just to get the setup working.
    """
    resp = bottle.HTTPResponse(body=make_test_gif(), status=200)
    resp.set_header("Content-Type", "image/gif")
    return resp


@bottle.route(
    "/api/v1/text", method="POST", apply=use_kwargs(TextGifRequest, location="json")
)
@enable_cors
# @json_response()
def get_text_gif(**text_gif_options):
    """
    Returns a scrolling text gif.
    """
    request_headers = bottle.request.headers
    logging.info(request_headers)
    try:
        if request_headers.get("Accept", "application/json") == "image/gif":
            img_resp = make_text_gif(**text_gif_options, returns="gif")
            bottle.response.add_header("Content-Type", "image/gif")
            return img_resp
        else:
            img_resp = make_text_gif(**text_gif_options, returns="json")
            return img_resp
    except Exception as err:
        message = f"Could not create image. The error is: {err}"
        logging.error({"message": message, "request": bottle.request})
        bottle.abort(500, message)


@bottle.error(404)
def error404(error):
    bottle.response.content_type = "application/json"
    return json.dumps({"error": "Could not find endpoint. Sorry sam. (404)"})


@bottle.error(500)
def error500(error):
    bottle.response.content_type = "application/json"
    logging.error(error)
    return json.dumps({"error": "Ah fuck. Server error. :( (500)"})
